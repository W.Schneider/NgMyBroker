import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { StocksOverviewComponent } from './stocks-overview/stocks-overview.component';
import { AddStocksComponent } from './add-stocks/add-stocks.component';
import { StocksService } from './stocks.service';
import { MaterializeModule } from 'angular2-materialize';
import { SimulationGameComponent } from './simulation-game/simulation-game.component';

@NgModule({
  declarations: [
    AppComponent,
    StocksOverviewComponent,
    AddStocksComponent,
    SimulationGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MaterializeModule
  ],
  providers: [StocksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
