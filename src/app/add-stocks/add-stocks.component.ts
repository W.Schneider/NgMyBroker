import { Component, OnInit } from '@angular/core';
import { StocksService } from '../stocks.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-stocks',
  templateUrl: './add-stocks.component.html',
  styleUrls: ['./add-stocks.component.css']
})
export class AddStocksComponent {

  stockName = '';

  constructor(private stocksService: StocksService, private router: Router) { }

  addStock() {
    this.stocksService.addStock(this.stockName);
    this.router.navigate(['/']);
  }

}
