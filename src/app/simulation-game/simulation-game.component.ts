import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-simulation-game',
  templateUrl: './simulation-game.component.html',
  styleUrls: ['./simulation-game.component.css']
})
export class SimulationGameComponent implements OnInit {

  money = 0;
  currency = 'USD';

  constructor() { }

  ngOnInit() {
  }

}
