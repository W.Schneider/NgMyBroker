import { Injectable, Input, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class StocksService {

  private stocksSubject = new Subject<Array<Quote>>();
  stocks = this.stocksSubject.asObservable();

  constructor(private httpClient: HttpClient) { }

  addStock(stock: string) {
    window.localStorage.setItem('stocks', this.getStocksFromLocalStorage() + '/' + stock);
  }

  loadStocks() {
    if (this.getStocksFromLocalStorage().length === 0) {
      this.stocksSubject.next([]);
    }
    const url = 'https://stockplaceholder.herokuapp.com/api/stocks' + this.getStocksFromLocalStorage();
    this.httpClient.get<Quote[]>(url).subscribe(quotes => {
      this.stocksSubject.next(quotes);
    });
  }

  getStocksFromLocalStorage() {
    const stocks = window.localStorage.getItem('stocks');
    if (stocks) {
      return stocks;
    } else {
      return '';
    }
  }

  removeStock(index: number) {
    const stocks = this.getStocksFromLocalStorage();
    const stockArray = stocks.split('/');
    stockArray.splice(0, 1);
    stockArray.splice(index, 1);
    window.localStorage.setItem('stocks', '/' + stockArray.join('/'));
  }
}

export interface Quote {
  symbol: string;
  name: string;
  change: string;
  currency: string;
  lastTradeDate: string;
  lastTradePriceOnly: string;
  changeinPercent: string;
  lastTradeTime: string;
}
