import { Component, OnInit } from '@angular/core';
import { StocksService, Quote } from '../stocks.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-stocks-overview',
  templateUrl: './stocks-overview.component.html',
  styleUrls: ['./stocks-overview.component.css']
})
export class StocksOverviewComponent implements OnInit {

  quotes: Quote[] = [];

  constructor(private stocksService: StocksService) {}

  ngOnInit() {

    this.stocksService.stocks.subscribe(quotes => {
      this.quotes = quotes;
      this.quotes.forEach(quote => {
        if (+quote.change > 0) {
          toast(`${quote.name} um ${quote.change} gestiegen`, 2000);
        }
      });
    });

    this.stocksService.loadStocks();
  }

  removeStock(index: number) {
    this.quotes.splice(index, 1);
    this.stocksService.removeStock(index);
  }

}
