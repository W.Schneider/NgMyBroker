import { Component, ViewChild } from '@angular/core';
import { StocksOverviewComponent } from './stocks-overview/stocks-overview.component';
import { StocksService } from './stocks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private stocksService: StocksService) {}

  refresh() {
    this.stocksService.loadStocks();
  }
}
