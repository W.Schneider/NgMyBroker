import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StocksOverviewComponent } from './stocks-overview/stocks-overview.component';
import { AddStocksComponent } from './add-stocks/add-stocks.component';

const routes: Routes = [
      { path: '', component: StocksOverviewComponent},
      { path: 'add', component: AddStocksComponent},
      { path: '**', component: StocksOverviewComponent}
    ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
